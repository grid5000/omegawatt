#!/usr/bin/perl -w
use strict;
use warnings;

my $file = $ARGV[0];
my $fh;
if (not defined ($file) or $file eq "-") {
    $fh = *STDIN;
} else {
    (-r $file) or die "Error, $file is not readable\n";
    if ($file =~ /.gz$/) {
        open $fh, "zcat $file |";
    } else {
        open $fh, "< $file";
    }
}
my $line;
while ($line = <$fh>) { 
    my @allvalues = split(',', $line);
    if ($line !~ /,OK,/) {
        print localtime($allvalues[2])."> ERROR IN LOG LINE\n";
    } elsif ($#allvalues != 33) {
        print localtime($allvalues[2])."> MALFORMED LOG LINE\n";
    } else {
        my @values = map { ($_ ne "")?sprintf("%06.1f", $_):"   N/A" } @allvalues[4..33];
        if ($allvalues[2] =~ /^\d+\.\d+$/) {
            print localtime($allvalues[2])."> ".join(", ", @values)."\n";
        } else {
            print localtime($allvalues[2])."> ERROR IN LOG LINE\n";
        }
    }
}
close $fh;
