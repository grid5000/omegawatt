CC	= gcc
SRC	= wattmetre_read.c
BIN	= $(SRC:.c=)
CFLAGS	= -g -D DEBUG=1 -lm

%: %.c
	$(CC) -o $@ $< $(CFLAGS)

all: $(BIN)

clean:
	rm $(BIN)

